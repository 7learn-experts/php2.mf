<?php
namespace App\Middleware;
use App\Core\Request;

class SanitizeMiddleware extends BaseMiddleware {
     public function handle(Request $request){
        $request->sanitizeAll();
     }
}