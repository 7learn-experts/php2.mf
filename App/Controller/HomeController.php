<?php
namespace App\Controller;
use App\Repo\PostRepo;
use Illuminate\Database\Eloquent\Collection;

class HomeController{
    public function index($request)
    {
        $postRepo = new PostRepo();
        $fivePosts = $postRepo->mostVisit(5);

        foreach ($fivePosts as $p){
            echo "$p->title : ($p->view_count)<br>";
        }
    }
}